`timescale 1ns / 1ps

module axispi_testbench();

/* BFM <--> DUT wires */
    /* Global signals */
    wire clk;  // System clk
    wire rst;  // System reset
        
    /* Write address and control channel */
    wire        wa_valid;
    wire        wa_ready;
    wire [31:0] wa_addr;
    // wire wa_chache; ??
    // wire wa_prot; ??
        
    /* Write data channel */
    wire        wd_valid;
    wire        wd_ready;
    wire [31:0] wd_data;
    wire [3:0]  wd_byte_en;
    
    /* Write response channel */
    wire wresp_valid;
    wire wresp_ready;
    wire [1:0] wresp_bresp;
        
    /* Read address and control channel */
    wire        ra_valid;
    wire        ra_ready;
    wire [31:0] ra_addr;
    // wire ra_cache; ??
    // wire ra_prot; ??
        
    /* Read data channel */
    wire        rd_valid;
    wire        rd_ready;
    wire [31:0] rd_data;
    wire [1:0] rd_rresp;

/* DUT <--> SPI EEPROM wires */
    wire SCLK;
    wire MISO;
    wire MOSI;
    wire nSS;
    wire nBusy;  // "active high Ready" or "active low Busy"
    wire IT_req;

/* Bus Functional Model instance */
axi_lite_bfm BFM (
        .clk(clk),
        .rst(rst),
        .wa_valid(wa_valid),
        .wa_ready(wa_ready),
        .wa_addr(wa_addr),
        // .wa_chache(wa_cache), ??
        // .wa_prot(wa_prot), ??
        .wd_valid(wd_valid),
        .wd_ready(wd_ready),
        .wd_data(wd_data),
        .wd_byte_en(wd_byte_en),
        .wresp_valid(wresp_valid),
        .wresp_ready(wresp_ready),
        .wresp_bresp(wresp_bresp),
        .ra_valid(ra_valid),
        .ra_ready(ra_ready),
        .ra_addr(ra_addr),
        // .ra_cache(ra_cache), ??
        // .ra_prot(ra_prot), ??
        .rd_valid(rd_valid),
        .rd_ready(rd_ready),
        .rd_data(rd_data),
        .IT_req(IT_req),
        .rd_rresp(rd_rresp)
    );

/* Peripherial instance */
toplevel DUT (
        /* AXI Lite */
        .clk(clk),
        .rst(rst),
        .wa_valid(wa_valid),
        .wa_ready(wa_ready),
        .wa_addr(wa_addr),
        // .wa_chache(wa_cache), ??
        // .wa_prot(wa_prot), ??
        .wd_valid(wd_valid),
        .wd_ready(wd_ready),
        .wd_data(wd_data),
        .wd_byte_en(wd_byte_en),
        // .wd_strb(wd_strb), ??
        .wresp_valid(wresp_valid),
        .wresp_ready(wresp_ready),
        .wresp_bresp(wresp_bresp),
        .ra_valid(ra_valid),
        .ra_ready(ra_ready),
        .ra_addr(ra_addr),
        // .ra_cache(ra_cache), ??
        // .ra_prot(ra_prot), ??
        .rd_valid(rd_valid),
        .rd_ready(rd_ready),
        .rd_data(rd_data),
        .rd_rresp(rd_rresp),
        
        /* SPI */
        .SCLK(SCLK),
        .MISO(MISO),
        .MOSI(MOSI),
        .nSS(nSS),
        .IT_req(IT_req)
        // .nBusy(nBusy)
    );

/* Microchip's 25LC010A 128x8bit SPI EEPROM memory model instance */
M25LC010A SPI_EEPROM (
        .RESET(rst),
        .SCK(SCLK),
        .SO(MISO),
        .SI(MOSI),
        .CS_N(nSS),
        .HOLD_N(1'b1),
        .WP_N(1'b1)      // Write protection?
    );

endmodule
