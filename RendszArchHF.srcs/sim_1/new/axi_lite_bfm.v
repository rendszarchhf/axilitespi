`timescale 1ns / 1ps

module axi_lite_bfm(
    /* Global signals */
    output clk,  // System clk
    output rst,  // System reset
        
    /* Write address and control channel */
    input         wa_ready,
    output        wa_valid,
    output [31:0] wa_addr,
    //output [2:0]  wa_prot,
    
    /* Write data channel */
    input         wd_ready,
    output        wd_valid,
    output [31:0] wd_data,
    output [3:0]  wd_byte_en,

    /* Write response channel */
    input [1:0] wresp_bresp, //OK 00
    input wresp_valid,
    output wresp_ready,


    /* Read address and control channel */
    input          ra_ready,
    output         ra_valid,
    output [31:0]  ra_addr,


    /* Read data channel */
    input [1:0]  rd_rresp,
    input [31:0] rd_data,
    input        rd_valid,
    output       rd_ready,
    
    /* Interrupt */
    input        IT_req
    );


/* Register addresses */
parameter BASE_ADDR          = 32'h0;
parameter STAT_CTRL_ADDR     = BASE_ADDR + 32'h00;
parameter CLOCK_DIV_ADDR     = BASE_ADDR + 32'h04;
parameter IT_FLAGS_ADDR      = BASE_ADDR + 32'h08;
parameter IT_ENABLE_ADDR     = BASE_ADDR + 23'h0C;
parameter EEPROM_MEMORY_BASE = BASE_ADDR + 32'h100;
parameter EEPROM_MEMORY_END  = EEPROM_MEMORY_BASE + 32'h80;

/* Input registers */
    reg        wa_ready_reg;
    reg        wd_ready_reg;
    reg [1:0]  wresp_bresp_reg;
    reg        wresp_valid_reg;
    reg        ra_ready_reg;
    reg        rd_valid_reg;
    reg [31:0] rd_data_reg;
    reg [ 1:0] rd_rresp_reg;
    reg        IT_req_reg;

/* Output registers */
    reg        wa_valid_reg;
    reg [31:0] wa_addr_reg;
    reg [3:0] wd_byte_en_reg;
    //reg [2:0] wa_prot_reg;
    reg        wd_valid_reg;
    reg        wresp_ready_reg;
    reg        ra_valid_reg;
    reg [31:0] wd_data_reg;
    reg [31:0] ra_addr_reg;
    reg        rd_ready_reg;
    reg        rst_reg;
    reg        clk_reg;



/* Initial conditions */
    initial
    begin
        wa_valid_reg <= 1'b0;
        wd_valid_reg <= 1'b0;
        wresp_ready_reg <= 1'b0;
        ra_valid_reg <= 1'b0;
        rd_ready_reg <= 1'b0;
        clk_reg <= 1'b0;
        // rst_reg <= 1'b0;
        rst_reg = 1'b1;
        
        #10000
        rst_reg = 1'b0;
        
        
        /* Test #1: Register reading */
        #10000
        read_transaction(STAT_CTRL_ADDR);
        
        #10000
        read_transaction(CLOCK_DIV_ADDR);
        
        
        /* Test #2: Register writing */
        #10000
        write_transaction(STAT_CTRL_ADDR, 32'h8000_0002);
        
        
        /* Test #3: Memory writing and reading */
        #10000
        write_transaction(EEPROM_MEMORY_BASE + 32'h10, 32'h42);
        wait(IT_req == 1'b1);
        
            /* Test #4: Interrupt clearing with reading the flags register */
            #10000
            read_transaction(IT_FLAGS_ADDR);
        
        #10000
        read_transaction(EEPROM_MEMORY_BASE + 32'h10);
        wait(IT_req == 1'b1);
        
            #10000
            read_transaction(IT_FLAGS_ADDR);
            
                    
        /* Test #5: Memory writing and reading with interrupt disabled */
        #10000
        write_transaction(IT_ENABLE_ADDR, 32'h0);
        
        #10000
        write_transaction(EEPROM_MEMORY_BASE + 32'h10, 32'h42);
        
        #6_000_000  // 6 ms wait, needed for the memory to finish its internal write
        
        read_transaction(EEPROM_MEMORY_BASE + 32'h10);
        
        
        /* Test #6: Multiple memory writing and -reading transactions */
        #10000
        write_transaction(IT_ENABLE_ADDR, 32'h1);
        
        #10000
        write_transaction(EEPROM_MEMORY_BASE + 32'h10, 32'h1);
        wait(IT_req == 1'b1);
        
            #10000
            read_transaction(IT_FLAGS_ADDR);
            
        #10000
        write_transaction(EEPROM_MEMORY_BASE + 32'h14, 32'h2);
        wait(IT_req == 1'b1);
        
            #10000
            read_transaction(IT_FLAGS_ADDR);
            
        #10000
        write_transaction(EEPROM_MEMORY_BASE + 32'h18, 32'h3);
        wait(IT_req == 1'b1);
        
            #10000
            read_transaction(IT_FLAGS_ADDR);
            
        #10000
        read_transaction(EEPROM_MEMORY_BASE + 32'h10);
        wait(IT_req == 1'b1);
        
            #10000
            read_transaction(IT_FLAGS_ADDR);
            
        #10000
        read_transaction(EEPROM_MEMORY_BASE + 32'h14);
        wait(IT_req == 1'b1);
        
            #10000
            read_transaction(IT_FLAGS_ADDR);
            
        #10000
        read_transaction(EEPROM_MEMORY_BASE + 32'h18);
        wait(IT_req == 1'b1);
        
            #10000
            read_transaction(IT_FLAGS_ADDR);

    end

/* Reset assignment */
assign rst = rst_reg;

/* Clock generation, 16 MHz, 625 ns period */
    always #312.5 clk_reg = ~clk_reg;
assign clk = clk_reg;


/* Input sampling */
always@(posedge clk)
begin
    wa_ready_reg <= wa_ready;
    wd_ready_reg <= wd_ready;
    wresp_valid_reg <= wresp_valid;
    ra_ready_reg <= ra_ready;
    rd_valid_reg <= rd_valid;
    IT_req_reg <= IT_req;
    wresp_bresp_reg <= wresp_bresp;
end


/* Bus transactions implemented using tasks */
    task write_transaction;
        input [31:0] addr, data;
        begin
            fork
                begin
                    #1 wa_addr_reg   = addr;
                    wa_valid_reg = 1'b1;
                    wait(wa_ready_reg == 1'b1);
                    #700 wa_valid_reg = 1'b0;
                end
                begin
                    #1 wd_data_reg  = data;
                    wd_byte_en_reg = 4'b1111;
                    wd_valid_reg = 1'b1;
                    wait(wd_ready_reg == 1'b1);
                    #700 wd_valid_reg = 1'b0;
                end
                begin
                    wresp_ready_reg = 1'b1;
                    wait(wresp_valid_reg == 1'b1);
                    #1;
                    wresp_ready_reg = 1'b0;
                    if (wresp_bresp_reg == 2'b00);
                end
            join
        end
    endtask
    
    assign wa_addr  = wa_addr_reg;    // Updating the outputs
    assign wa_valid = wa_valid_reg;
    assign wd_data   = wd_data_reg;
    assign wd_valid = wd_valid_reg;
    assign wresp_ready = wresp_ready_reg;
    assign wd_byte_en = wd_byte_en_reg;


    task read_transaction;
        input [31:0] addr;
        begin
            fork
                begin
                    #1 ra_addr_reg   = addr;
                    ra_valid_reg = 1'b1;
                    wait(ra_ready_reg == 1'b1);
                    #700 ra_valid_reg = 1'b0;
                end
            
                begin
                    rd_ready_reg = 1'b1;
                    wait(rd_valid_reg == 1'b1);  // Waiting for the data
                    rd_data_reg    = rd_data;    // Saving data from input reg to inner memory
                    rd_rresp_reg = rd_rresp;
                    rd_ready_reg = 1'b0;
                    if (rd_rresp_reg == 2'b00);
                end
            join
        end
    endtask
    
    assign ra_addr  = ra_addr_reg;    // Updating th outputs
    assign ra_valid = ra_valid_reg;
    assign rd_ready = rd_ready_reg;

endmodule
