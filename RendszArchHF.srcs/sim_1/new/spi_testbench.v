`timescale 1ns / 1ps

module spi_testbench();

    /* Global signals */
    reg clk;
    reg rst;

    /* SPI IF signals */
    wire        MISO;
    wire        SCLK;
    wire        MOSI;
    wire [7:0]  nSS;    // acitve low slave select
    
    /* Signals towards AXI Lite bus if. */
    reg  [31:0]  w_addr_a2s;
    reg          w_en_a2s;
    reg  [3:0]   byte_en_a2s;
    reg  [31:0]  w_data_a2s;
    reg  [31:0]  r_addr_a2s;
    reg          r_en_a2s;
    wire [31:0]  r_data_s2a;
    wire         r_ack_s2a;
    wire         w_ack_s2a;
    wire         IT_req;

spi_if DUT(
    /* Global signals */
    .clk(clk),
    .rst(rst),

    /* SPI IF signals */
    .MISO(MISO),
    .SCLK(SCLK),
    .MOSI(MOSI),
    .nSS(nSS),    // acitve low slave select
    
    /* Signals towards AXI Lite bus if. */
    .w_addr_a2s(w_addr_a2s),
    .w_en_a2s(w_en_a2s),
    .byte_en_a2s(byte_en_a2s),
    .w_data_a2s(w_data_a2s),
    .r_addr_a2s(r_addr_a2s),
    .r_en_a2s(r_en_a2s),
    .r_data_s2a(r_data_s2a),
    .r_ack_s2a(r_ack_s2a),
    .w_ack_s2a(w_ack_s2a),
    .IT_req(IT_req)       // Processor interrupt
    );

/* Microchip's 25LC010A 128x8bit SPI EEPROM memory model instance */
M25LC010A SPI_EEPROM (
        .RESET(rst),
        .SCK(SCLK),
        .SO(MISO),
        .SI(MOSI),
        .CS_N(nSS[0]),   // Memory's slave select signal is nSS[0]
        .HOLD_N(1'b1),
        .WP_N(1'b1)      // Write protection?
    );

/* Register addresses */
parameter BASE_ADDR               = 32'h0;
parameter STAT_CTRL_ADDR          = BASE_ADDR + 32'h00;
parameter CLOCK_DIV_ADDR          = BASE_ADDR + 32'h04;
parameter IT_FLAGS_ADDR           = BASE_ADDR + 32'h08;
parameter IT_ENABLE_ADDR          = BASE_ADDR + 32'h0C;
parameter EEPROM_MEMORY_BASE      = BASE_ADDR + 32'h100;
parameter EEPROM_MEMORY_END       = EEPROM_MEMORY_BASE + 32'h80;

initial
begin
    /* System reset and clock init */
    clk = 1'b0;
    rst = 1'b1;
    #100
    rst = 1'b0;


    /* TEST #1: Writing a register */
    #10
    w_addr_a2s  = STAT_CTRL_ADDR;  // Writing the status/control register
    w_data_a2s  = 32'h1;           // with CSn = 0 (active) and EN = 1 (active) values
    byte_en_a2s = 4'b1111;         // Indicating that we treat the data as a whole 32 bit value
    #3
    w_en_a2s    = 1'b1;            // Signaling to the SPI unit that we would like to write and
                                   // address and data values are valid
    #11
    w_en_a2s    = 1'b0;            // After one system clock SPI unit sampled our output, write en. does not need to be active
    wait(w_ack_s2a == 1'b1);       // Waiting for write acknowledge to be active (write transaction successfully ended)

    
    /* TEST #2: Reading a register */
	#10
    r_addr_a2s = CLOCK_DIV_ADDR;   // Reading the clock division register
    #3
    r_en_a2s = 1'b1;               // Signaling to the SPI unit that we would like to read and address is valid
    #11
    r_en_a2s = 1'b0;               // After one system clock SPI unit sampled our output, read en. does not need to be active
    wait(r_ack_s2a == 1'b1);       // Waiting for read acknowledge to be active (read transaction successfully ended)
    
    /* TEST #3: Writing one byte into the memory */
    #10
    w_addr_a2s  = EEPROM_MEMORY_BASE + 32'h10;  // Writing to somewhere in the memory
    w_data_a2s  = 32'h42;                        // Test value
    byte_en_a2s = 4'b1111;                       // Indicating that we treat the data as a whole 32 bit value
    #3
    w_en_a2s    = 1'b1;            // Signaling to the SPI unit that we would like to write and
                                   // address and data values are valid
    #11
    w_en_a2s    = 1'b0;            // After one system clock SPI unit sampled our output, write en. does not need to be active
    wait(w_ack_s2a == 1'b1);       // Waiting for write acknowledge to be active (write transaction successfully ended)

    #6_000_000

    /* TEST #4: Reading back that one byte from memory (see TEST #3) */
	#10
    r_addr_a2s = EEPROM_MEMORY_BASE + 32'h10;   // Reading back from the memory
    #3
    r_en_a2s = 1'b1;               // Signaling to the SPI unit that we would like to read and address is valid
    #11
    r_en_a2s = 1'b0;               // After one system clock SPI unit sampled our output, read en. does not need to be active
    wait(r_ack_s2a == 1'b1);       // Waiting for read acknowledge to be active (read transaction successfully ended)

    #1_000_000

    /* TEST #5: Writing multiple bytes into the memory */
    /* First write cycle */
    #10
    w_addr_a2s  = EEPROM_MEMORY_BASE + 32'h10;  // Writing to somewhere in the memory
    w_data_a2s  = 32'h01;                       // Test value
    byte_en_a2s = 4'b1111;                      // Indicating that we treat the data as a whole 32 bit value
    #3
    w_en_a2s    = 1'b1;            // Signaling to the SPI unit that we would like to write and
                                   // address and data values are valid
    #11
    w_en_a2s    = 1'b0;            // After one system clock SPI unit sampled our output, write en. does not need to be active
    wait(w_ack_s2a == 1'b1);       // Waiting for write acknowledge to be active (write transaction successfully ended)

    #6_000_000
    /* Second write cycle */
    #10
    w_addr_a2s  = EEPROM_MEMORY_BASE + 32'h14;  // Writing to "somewhere+1" in the memory
    w_data_a2s  = 32'h02;                       // Test value
    byte_en_a2s = 4'b1111;                      // Indicating that we treat the data as a whole 32 bit value
    #3
    w_en_a2s    = 1'b1;            // Signaling to the SPI unit that we would like to write and
                                   // address and data values are valid
    #11
    w_en_a2s    = 1'b0;            // After one system clock SPI unit sampled our output, write en. does not need to be active
    wait(w_ack_s2a == 1'b1);       // Waiting for write acknowledge to be active (write transaction successfully ended)

    #6_000_000

    /* Third write cycle */
    #10
    w_addr_a2s  = EEPROM_MEMORY_BASE + 32'h18;  // Writing to "somewhere+2" in the memory
    w_data_a2s  = 32'h03;                       // Test value
    byte_en_a2s = 4'b1111;                      // Indicating that we treat the data as a whole 32 bit value
    #3
    w_en_a2s    = 1'b1;            // Signaling to the SPI unit that we would like to write and
                                   // address and data values are valid
    #11
    w_en_a2s    = 1'b0;            // After one system clock SPI unit sampled our output, write en. does not need to be active
    wait(w_ack_s2a == 1'b1);       // Waiting for write acknowledge to be active (write transaction successfully ended)

    #6_000_000

    /* TEST #6: Reading back multiple bytes from memory (see TEST #5) */
    /* First reading cycle */
	#10
    r_addr_a2s = EEPROM_MEMORY_BASE + 32'h10;   // Reading back first byte from the memory
    #3
    r_en_a2s = 1'b1;               // Signaling to the SPI unit that we would like to read and address is valid
    #11
    r_en_a2s = 1'b0;               // After one system clock SPI unit sampled our output, read en. does not need to be active
    wait(r_ack_s2a == 1'b1);       // Waiting for read acknowledge to be active (read transaction successfully ended)

    #1_000_000

    /* Second reading cycle */
	#10
    r_addr_a2s = EEPROM_MEMORY_BASE + 32'h14;   // Reading back second byte from the memory
    #3
    r_en_a2s = 1'b1;               // Signaling to the SPI unit that we would like to read and address is valid
    #11
    r_en_a2s = 1'b0;               // After one system clock SPI unit sampled our output, read en. does not need to be active
    wait(r_ack_s2a == 1'b1);       // Waiting for read acknowledge to be active (read transaction successfully ended)

    #1_000_000

    /* Third reading cycle */
	#10
    r_addr_a2s = EEPROM_MEMORY_BASE + 32'h18;   // Reading back third byte from the memory
    #3
    r_en_a2s = 1'b1;               // Signaling to the SPI unit that we would like to read and address is valid
    #11
    r_en_a2s = 1'b0;               // After one system clock SPI unit sampled our output, read en. does not need to be active
    wait(r_ack_s2a == 1'b1);       // Waiting for read acknowledge to be active (read transaction successfully ended)

end

always #5 clk = ~clk;

endmodule
