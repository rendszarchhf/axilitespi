`timescale 1ns / 1ps

module spi_if(
    /* Global signals */
    input         clk,
    input         rst,

    /* SPI IF signals */
    input         MISO,
    output        SCLK,
    output        MOSI,
    output [7:0]  nSS,    // Acitve low slave select
    
    /* Signals towards AXI Lite bus if. */
    input  [31:0] w_addr_a2s,
    input         w_en_a2s,
    input  [ 3:0] byte_en_a2s,
    input  [31:0] w_data_a2s,
    input  [31:0] r_addr_a2s,
    input         r_en_a2s,
    output [31:0] r_data_s2a,
    output        r_ack_s2a,
    output        w_ack_s2a,
    output        IT_req       // Processor interrupt
    );
    
/* Registers of Programming Interface */
reg [31:0] stat_ctrl = 32'h1;          // Holds Busy bit (read only), Peripherial enable bit, slave select bits (write only)
reg [31:0] clock_div = 32'h04;         // SCLK freq. will be sys. clk / 2*clock_div, initial value is 4 (1/8 clk freq.) (write only)
reg [31:0] it_flags  = 32'h0;          // Interrupt flags (read only, read clears IT flag)
reg [31:0] it_enable = 32'h1;          // Interrupt generation can be enabled (write only)
reg [ 7:0] nSS_reg   =  8'b1111_1111;  // Slave select register (active low, one bit for every slave device)

/* Internal registers */
reg [ 7:0] shift_data;                  // Holds data bits and shifts them in/out
reg        memory_read_en  = 1'b0;      // Flag to activate memory reading processes 
reg        memory_write_en = 1'b0;      // Flag to activate memory writing processes
reg        shift_en        = 1'b0;      // Enables right shift of shift_data
reg        cntr_en         = 1'b0;      // Enables the bit counter
reg [ 4:0] shift_cntr      = 5'b00000;  // Initial value of bit counter
reg [31:0] valid_data;                  // Holds valid sent/received data
reg [31:0] valid_address;               // Holds valid received address
reg [31:0] sclk_gen_cntr;               // Counter for serial (SPI) clock generation
reg        writing_state   = 1'b0;      // Indicates that the requested "write" type transaction started 
reg        reading_state   = 1'b0;      // Indicates that the requested "read" type transaction started
reg        mem_w_en_done   = 1'b0;      // Indicates that the "write enable" command gone out (needed before every "write" command/cycle)
reg [18:0] wait_cntr       = 0;         // Measures elapsed time for short (5 us) and long (5.1 ms) waits.
                                        // These waits are needed after "write enable" and "write" cycles, respectively.
reg        short_wait_cntr_en = 1'b0;   // 0 to 1 transition starts a short wait
reg        long_wait_cntr_en  = 1'b0;   // 0 to 1 transition starts a long wait. While Long wait is active, new SPI transactions are blocked.

/* Output registers */
reg         SCLK_reg       =  1'b0;
reg [31:0]  r_data_s2a_reg = 32'b0;
reg         r_ack_s2a_reg  =  1'b0;
reg         w_ack_s2a_reg  =  1'b0;
reg         IT_req_reg     =  1'b0;

/* Instruction definitions based on EEPROM datasheet */
parameter READ  = 8'b0000_0011;
parameter WRITE = 8'b0000_0010;
parameter WRDI  = 8'b0000_0100;
parameter WREN  = 8'b0000_0110;
parameter RDSR  = 8'b0000_0101;
parameter WRSR  = 8'b0000_0001;

/* Register addresses */
parameter BASE_ADDR          = 32'h0;
parameter STAT_CTRL_ADDR     = BASE_ADDR + 32'h00;
parameter CLOCK_DIV_ADDR     = BASE_ADDR + 32'h04;
parameter IT_FLAGS_ADDR      = BASE_ADDR + 32'h08;
parameter IT_ENABLE_ADDR     = BASE_ADDR + 23'h0C;
parameter EEPROM_MEMORY_BASE = BASE_ADDR + 32'h100;
parameter EEPROM_MEMORY_END  = EEPROM_MEMORY_BASE + 32'h80;

/* 
** List of processes implemented below
**
** - Handling "read" type requests:
**   > System clock sensitive.
**   > Active when r_en_a2s input signal is active.
**   > Handles reading from registers or SPI memory.
**
** - Handling "write" type requests:
**   > System clock sensitive.
**   > Active when w_en_a2s input signal is active.
**   > Handles writing to registers or SPI memory.
**
** - Serial (SPI) clock generation:
**   > System clock sensitive.
**   > Active when cntr_en signal is active.
**   > Generates serial clock based on system clock and the value of the clock
**     division register. Output clock freq. will be (sys. clk freq.) / (2*clock_div)
**
** - Process to load the shift register:
**   > Serial (SPI) clock sensitive.
**   > Active when memory_read_en or memory_write_en signal is active. Requires 
**     shift_en signal to be inactive in order to work as expected.
**   > Turns off shift_en signal if loading of new byte will be needed. Loads 
*      appropriate instructions, address or data then turns on shift_en.
**
** - Wait counter:
**   > System clock sensitive.
**   > Active either when short or long wait is enabled.
**   > Implements a counter for short and long wait periods. These waits are needed for
**     the proper operation of SPI memory (the 5.1 ms long is the device's internal write
**     cycle time plus 0.1 ms).
**
** - Shift register:
**   > Serial (SPI) clock sensitive.
**   > Active when shift_en signal is active.
**   > Implements right shifting of 8 bit in/out SPI shift register.
**
** - Shift counter:
**   > Serial (SPI) clock sensitive.
**   > Active when shift_cntr signal is active (shift counter enable signal).
**   > Counts the bits of SPI transaction to provide information to other processes about
**     byte and transaction boundaries.
**
** - SPI to AXI output assignment and transaction reset:
**   > System clock sensitive.
**   > Active when shift counter indicates that 3 bytes where shifted out hence a transaction
**     is about to end.
**   > Updates SPI to AXI interface signals with transaction results and if IRQ is enabled,
**     generates interrupt request. Besides these, it resets SPI transaction related registers
**     into there default state.
*/

/* Handling "read" type requests */
always@(posedge clk)
begin
  if (rst)
    begin
      r_data_s2a_reg <= 0;
      r_ack_s2a_reg  <= 0;
    end
  else
    begin
      r_ack_s2a_reg <= 1'b0;
      if (r_en_a2s && ~stat_ctrl[31])  // Read enable and we're not busy
      begin
        reading_state <= 1'b1;
        stat_ctrl[31] <= 1'b1;         // Set busy flag
        
        valid_address = r_addr_a2s;    // Read the address from AXI
                                       // with blocking assignment to use
                                       // valid_address's new value in case statement
        case (valid_address)

            /* Reading peripherial registers */
            STAT_CTRL_ADDR:
              begin
                r_data_s2a_reg <= stat_ctrl;
                r_ack_s2a_reg  <= 1'b1;
                stat_ctrl[31]  <= 1'b0;
                reading_state  <= 1'b0;
              end
            CLOCK_DIV_ADDR:
              begin
                r_data_s2a_reg <= clock_div;
                r_ack_s2a_reg  <= 1'b1;
                stat_ctrl[31]  <= 1'b0;
                reading_state  <= 1'b0;
              end
            IT_FLAGS_ADDR:
              begin
                r_data_s2a_reg <= it_flags;
                r_ack_s2a_reg  <= 1'b1;
                stat_ctrl[31]  <= 1'b0;
                reading_state  <= 1'b0;
                it_flags       <= 32'b0;
                IT_req_reg     <= 1'b0;
              end
            IT_ENABLE_ADDR:
              begin
                r_data_s2a_reg <= it_enable;
                r_ack_s2a_reg  <= 1'b1;
                stat_ctrl[31]  <= 1'b0;
                reading_state  <= 1'b0;
              end

            /* Read address is out of register address space */
            default:
              begin

                /* Reading from SPI memory */
                if ((valid_address > EEPROM_MEMORY_BASE) && (valid_address < EEPROM_MEMORY_END))
                  begin
                    memory_read_en <= 1'b1;          // Enabling memory read process
                  end

                /* Read address is out of memory address space */
                else
                  begin
                    r_data_s2a_reg <= 32'hdeadbeef;
                    r_ack_s2a_reg  <= 1'b1;
                    stat_ctrl[31]  <= 1'b0;
                    reading_state  <= 1'b0;
                  end
               end
         endcase
       end
   end   
end

/* Handling "write" type requests */
always@(posedge clk)
begin
  if (rst)
    begin
      w_ack_s2a_reg <= 0;
    end
  else
    begin
      w_ack_s2a_reg <= 1'b0;
      if (w_en_a2s && ~stat_ctrl[31] && (byte_en_a2s == 4'b1111))  // Write enable and we're not busy
      begin
        writing_state <= 1'b1;
        stat_ctrl[31] <= 1'b1;       //Set busy flag
        valid_address = w_addr_a2s;  // Read the address from AXI
                                     // with blocking assignment to use
                                     // valid_address's new value in case statement
        valid_data = w_data_a2s;     // Reading the data from AXI with blocking assignment
        case (valid_address)

            /* Writing peripherial registers */
            STAT_CTRL_ADDR:
              begin
                stat_ctrl[1:0] <= valid_data[1:0];
                w_ack_s2a_reg <= 1'b1;
                stat_ctrl[31] <= 1'b0;
                writing_state <= 1'b0;
              end
            CLOCK_DIV_ADDR:
              begin
                clock_div <= valid_data;
                w_ack_s2a_reg <= 1'b1;
                stat_ctrl[31] <= 1'b0;
                writing_state <= 1'b0;
              end
            IT_FLAGS_ADDR:
              begin
                it_flags <= valid_data;
                w_ack_s2a_reg <= 1'b1;
                stat_ctrl[31] <= 1'b0;
                writing_state <= 1'b0;
              end
            IT_ENABLE_ADDR:
              begin
                it_enable <= valid_data;
                w_ack_s2a_reg <= 1'b1;
                stat_ctrl[31] <= 1'b0;
                writing_state <= 1'b0;
              end

            /* Write address is out of register address space */
            default:
              begin

                /* Writing to SPI memory */
                if ((valid_address > EEPROM_MEMORY_BASE) && (valid_address < EEPROM_MEMORY_END))
                  begin
                    memory_write_en <= 1'b1;          // Enabling memory write process
                  end

                /* Write address is out of memory address space */
                else
                  begin
                    w_ack_s2a_reg <= 1'b1;
                    stat_ctrl[31] <= 1'b0;
                    writing_state <= 1'b0;
                  end
               end
         endcase
       end
   end   
end

/* Serial (SPI) clock generation */
always@(posedge clk)
begin
    if (rst) begin
        sclk_gen_cntr <= 0;
        SCLK_reg <= 0;
    end 
    else if (memory_read_en || memory_write_en)
        if (sclk_gen_cntr == (clock_div - 1))
          begin
            sclk_gen_cntr <= 32'h0;
            SCLK_reg      <= ~SCLK_reg;
          end
        else
            sclk_gen_cntr <= sclk_gen_cntr + 1;
    else begin
        sclk_gen_cntr <= 0;
        SCLK_reg <= 0;
    end    
end

/* Process to load the shift register */
always @(negedge SCLK_reg)
begin
    /* If shifting of instruction or address is about to end,
       loading of new byte will be needed and shifting must be turned off */
    if ((shift_cntr == 5'b00110) || (shift_cntr == 5'b01110))
    begin
        shift_en <= 1'b0;
    end

    /* Read cycle */
    if (memory_read_en == 1'b1)
    begin
        case (shift_cntr)
            5'b00000:   // Beginning of a read cycle
            begin
                shift_data <= READ;
                nSS_reg    <= 8'b1111_1110;  // Active low slave select, 
                                             // memory's select signal is nSS_reg[0]
		        shift_en   <= 1'b1;
                cntr_en    <= 1'b1;
            end
            5'b00111:   // Instruction byte is already out, address comes next
            begin
                shift_data <= valid_address;
                shift_en   <= 1'b1;
            end
            5'b01111:   // Instr. and address is already out, data comes next
            begin
                shift_data <= 8'b0000_0000;  // Dummy data, valid data comes from memory
                shift_en   <= 1'b1;
            end
        endcase
    end

    /* Write cycle */
    else if (memory_write_en == 1'b1)
    begin
        case (shift_cntr)
            5'b00000:   // Beginning of a write cycle
            begin
                shift_data <= mem_w_en_done ? WRITE : WREN;  // If "write enable" is not done, then start it,
                                                             // otherwise start the "write" cycle.
                nSS_reg    <= 8'b1111_1110;  // Active low slave select, 
                                             // memory's select signal is nSS_reg[0]
                shift_en   <= 1'b1;
                cntr_en    <= 1'b1;
            end
            5'b00111:   // Instruction byte is already out, address comes next
            begin
                if (mem_w_en_done)                  // If the "write enable" cycle is already done it means its a
                  begin                             // "write" cycle, so...
                    shift_data <= valid_address;    // ...load the address
                    shift_en   <= 1'b1;
                  end
                else                                // If "write enable" hasn't done, then deactivate the chip select
                  begin                             // and wait for a short time before starting the real "write" cycle.
                    nSS_reg         <= 8'b1111_1111;  // Active low slave select, 
                                                      // memory's select signal is nSS_reg[0]
                    mem_w_en_done   <= 1'b1;          // Signaling that "write enable" cycle has done
                    shift_en        <= 1'b0;          // Stop shifting and bit counting
                    cntr_en         <= 1'b0;
                    memory_write_en <= 1'b0;          // Disabling even the write processes (timer will switch it back)
                    short_wait_cntr_en    <= 1'b1;    // Starting the short wait timer
                  end
            end
            5'b01111:   // Instr. and address are already out, data comes next
            begin
                shift_data <= valid_data;   // Third phase of a normal "write" cycle: sending the data
                shift_en   <= 1'b1;
            end
        endcase
    end
end

/* Wait counter */
always@(posedge clk)
begin
    if (short_wait_cntr_en)
      begin
        if (wait_cntr == 90)              // If short wait is over (5 us in case of a 100 MHz system clock)
          begin
            wait_cntr          <= 0;      // Clearing the counter
            short_wait_cntr_en <= 1'b0;   // Disabling it(self)
            memory_write_en    <= 1'b1;   // Enabling the write process to countinue with real "write" cycle
          end
        else
            wait_cntr <= wait_cntr + 1;
      end
    else if (long_wait_cntr_en)
      begin
        if (wait_cntr == 9000)            // If long wait is over (5.1 ms in case of a 100 MHz system clock)
          begin
            wait_cntr         <= 0;       // Clearing the counter
            long_wait_cntr_en <= 1'b0;    // and disabling it(self).
                                          // Enabling write process is not task here because its already ended,
                                          // we just waited to ensure that the memory will do its internal write
                                          // cycle without interruption.
          end
        else
            wait_cntr <= wait_cntr + 1;
      end
    else
        wait_cntr <= 0;
end

/* Shift register */
always@(negedge SCLK_reg)
begin
    if (shift_en == 1'b1)
    begin
        shift_data <= { shift_data[6:0], MISO };        
    end
end

/* Shift counter */
always@(negedge SCLK_reg)
begin
    /* If counter is enabled then count */
    if (cntr_en)
      begin
        shift_cntr <= shift_cntr + 1;
      end
    else
        shift_cntr <= 0;
end

/* SPI to AXI output assignment and transaction reset */
always@(posedge clk)
begin
    /* If 3 bytes were shifted out */
    if (shift_cntr == 5'b11000)
      begin
        if (reading_state == 1'b1)
          begin
            reading_state   = 1'b0;        // Reading is over
            valid_data[7:0] = shift_data;  // Transfering received data (8 bit) from shift reg. (8 bit) to valid data reg. (32 bit)
            r_data_s2a_reg  = valid_data;  // Updating output ports
            r_ack_s2a_reg   = 1'b1;        // Read transaction finished
            short_wait_cntr_en = 1'b1;     // Short wait starts to prevent the read enable signal to start again the same reading cycle
                                           // Without this wait, the AXI if. module samples the acknowledge signal at the same time the
                                           // samples again the read enable again incorrectly.
          end
        else if (writing_state == 1'b1)
          begin
            writing_state <= 1'b0;
            w_ack_s2a_reg <= 1'b1;
            long_wait_cntr_en = 1'b1;      // Long wait starts when a "write" cycle ends to ensure that the memory can do its internal write
                                           // cycle without interruption.
          end

        /* Resetting SPI transaction related registers */
        if (long_wait_cntr_en == 1'b0
            && short_wait_cntr_en == 1'b0) // The activity of the wait counters disables the clearing of the shift counter
          begin
          if (it_enable[0])                // If interrupt request is enabled
              begin
                it_flags[0] <= 1'b1;       // Then set IRQ flag
                IT_req_reg  <= 1'b1;       // and IRQ output
              end                          // This will prevent the start of a new SPI transaction.
            shift_cntr      <= 5'b00000;
            mem_w_en_done   <= 1'b0;
          end
        cntr_en         <= 1'b0;           // All other SPI transaction related registers can be cleared
        memory_read_en  <= 1'b0;
        memory_write_en <= 1'b0;
        shift_en        <= 1'b0;
        nSS_reg         <= 8'b1111_1111;
        stat_ctrl[31]   <= 1'b0;
      end
end

assign MOSI       = shift_data[7];
assign nSS        = nSS_reg;
assign r_data_s2a = r_data_s2a_reg;
assign r_ack_s2a  = r_ack_s2a_reg;
assign w_ack_s2a  = w_ack_s2a_reg;
assign SCLK       = SCLK_reg;
assign IT_req     = IT_req_reg;

endmodule
