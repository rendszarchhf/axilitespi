`timescale 1ns / 1ps

module toplevel(
    /* AXI Lite bus if. signals */
        /* Global signals */
         input clk,  // System clk
         input rst,  // System reset
        
        /* Write address and control channel */
         input        wa_valid,
         output       wa_ready,
         input [31:0] wa_addr,
         //input [2:0]  wa_prot,
        
        /* Write data channel */
         input         wd_valid,
         output        wd_ready,
         input [31:0]  wd_data,
         input [3:0]   wd_byte_en,
    
        /* Write response channel */
         input  wresp_valid,
         output wresp_ready,
         output wresp_bresp,
        
        /* Read address and control channel */
         output       ra_valid,
         input        ra_ready,
         input [31:0] ra_addr,
        // input ra_prot, ??
        
        /* Read data channel */
        input         rd_ready,
        output        rd_valid,
        output [31:0] rd_data,
        output [1:0] rd_rresp,
    
    /* SPI if. signals */
        input MISO,
        output SCLK,
        output MOSI,
        output nSS,
        output IT_req
    //output nBusy  // "active high Ready" or "active low Busy"
    );
        wire clk;  // System clk
        wire rst;  // System reset
        
        /* Write address and control channel */
        wire [31:0] wa_addr;
        wire        wa_valid;
        //wire [2:0]  wa_prot,
        wire        wa_ready;

        
        /* Write data channel */
        wire [3:0]  wd_byte_en;
        wire [31:0] wd_data;
        wire        wd_valid;
        wire       wd_ready;

        /* Write response channel */
        wire         wresp_ready;
        wire        wresp_valid;
        wire [1:0]  wresp_bresp;
        
        /* Read address and control channel */
        wire [31:0]  ra_addr;
        wire         ra_valid;
        wire        ra_ready;

        wire [2:0]   ra_prot;
        
        /* Read data channel */
        wire         rd_ready;
        wire        rd_valid;

        wire [31:0] rd_data;
        wire [1:0]  rd_rresp;
    /* Signals towards SPI if. */
        /* Write State Machine */
        wire w_ack_s2a;
        wire [31:0] w_addr_a2s;
        wire w_en_a2s;
        wire [3:0] byte_en_a2s;
        wire [31:0] w_data_a2s;

        /* Read State Machine */
        wire [31:0] r_data_s2a;
        wire r_ack_s2a;
        wire [31:0] r_addr_a2s;
        wire r_en_a2s;
        
        wire MISO;
        wire MOSI;
        wire SCLK;
        wire nSS;
/* Instantiating AXI Lite bus if. subsystem and connecting/routing its signals */
/* TODO */
axi_lite_if AXI_LITE (
        /* AXI Lite */
        .clk(clk),
        .rst(rst),
        .wa_valid(wa_valid),
        .wa_ready(wa_ready),
        .wa_addr(wa_addr),
        //.wa_prot(wa_prot),
        .wd_valid(wd_valid),
        .wd_ready(wd_ready),
        .wd_data(wd_data),
        .wd_byte_en(wd_byte_en),
        .wresp_valid(wresp_valid),
        .wresp_ready(wresp_ready),
        .wresp_bresp(wresp_bresp),
        .ra_valid(ra_valid),
        .ra_ready(ra_ready),
        .ra_addr(ra_addr),
        //.ra_prot(ra_prot),
        .rd_valid(rd_valid),
        .rd_ready(rd_ready),
        .rd_data(rd_data),
        .rd_rresp(rd_rresp),
        .w_ack_s2a(w_ack_s2a),
        .w_addr_a2s(w_addr_a2s),
        .w_en_a2s(w_en_a2s),
        .byte_en_a2s(byte_en_a2s),
        .w_data_a2s(w_data_a2s),
        .r_data_s2a(r_data_s2a),
        .r_ack_s2a(r_ack_s2a),
        .r_addr_a2s(r_addr_a2s),
        .r_en_a2s(r_en_a2s)
    );
    
    spi_if SPI (
    .clk(clk),
    .rst(rst),
    .MISO(MISO),
    .MOSI(MOSI),
    .SCLK(SCLK),
    .nSS(nSS),
    .w_ack_s2a(w_ack_s2a),
    .w_addr_a2s(w_addr_a2s),
    .w_en_a2s(w_en_a2s),
    .byte_en_a2s(byte_en_a2s),
    .w_data_a2s(w_data_a2s),
    .r_data_s2a(r_data_s2a),
    .r_ack_s2a(r_ack_s2a),
    .r_addr_a2s(r_addr_a2s),
    .r_en_a2s(r_en_a2s),
    .IT_req(IT_req)
    );
/* Instantiating SPI if. subsystem and connecting/routing its signals */
/* TODO */

endmodule