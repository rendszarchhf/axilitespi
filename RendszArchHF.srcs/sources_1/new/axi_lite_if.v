`timescale 1ns / 1ps

module axi_lite_if(
    /* AXI Lite bus signals */
        /* Global signals */
         input clk,  // System clk
         input rst,  // System reset
        
        /* Write address and control channel */
        input [31:0] wa_addr,
        input        wa_valid,
        output       wa_ready,

        
        /* Write data channel */
        input [3:0]  wd_byte_en,
        input [31:0] wd_data,
        input        wd_valid,
        output       wd_ready,

        /* Write response channel */
        input         wresp_ready,
        output        wresp_valid,
        output [1:0]  wresp_bresp,
        
        /* Read address and control channel */
        input [31:0]  ra_addr,
        input         ra_valid,
        output        ra_ready,
        
        /* Read data channel */
        input         rd_ready,
        output        rd_valid,

        output [31:0] rd_data,
        output [1:0]  rd_rresp,
    
    /* Signals towards SPI if. */
        /* Write State Machine */
        input w_ack_s2a,
        output [31:0] w_addr_a2s,
        output w_en_a2s,
        output [3:0] byte_en_a2s,
        output [31:0] w_data_a2s,

        /* Read State Machine */
        input [31:0] r_data_s2a,
        input r_ack_s2a,
        output [31:0] r_addr_a2s,
        output r_en_a2s
        );
        
/* Write state machine */
    /* States */
    parameter WRADDR_WAIT = 2'b00;  // Gray-coded states
    parameter WRDATA_WAIT = 2'b01;
    parameter WRITE       = 2'b11;
    parameter WRRESP_SEND = 2'b10;
    
    /* Registers */
    reg  [1:0] w_state_reg;
    reg  [1:0] w_next_state;
    
    reg [31:0] wa_addr_reg;             // Input buffer register
    reg [31:0] wd_data_reg;             // Input buffer register
    reg [3:0]  wd_byte_en_reg;          // Input buffer register
    reg        w_en_a2s_reg;           // Output buffer register
    reg        wa_valid_reg = 1'b0;     // Input buffer register
    reg        wd_valid_reg = 1'b0;     // Input buffer register
    reg        wresp_valid_reg = 1'b0;  // Input buffer register
    reg [1:0]  wresp_bresp_reg = 2'b11; // Output buffer register
    reg        wa_ready_reg = 1'b0;     // Output buffer register
    reg        wd_ready_reg = 1'b0;     // Output buffer register
    reg        wresp_ready_reg = 1'b0;  // Input buffer register
    
    /* State register */
    always@(posedge clk)
    begin
        if(rst)
          begin
            w_state_reg <= WRADDR_WAIT;
          end
        else
          begin
            w_state_reg <= w_next_state;
          end
    end
    
    always@(posedge clk)
    begin
        wa_valid_reg <= wa_valid;
        wd_valid_reg <= wd_valid;
        wresp_ready_reg <= wresp_ready;
        
        ra_valid_reg <= ra_valid;
        rd_ready_reg <= rd_ready;
    end
    
    /* State changes (combinational logic) */
    always@(*)
    begin
        case(w_state_reg)
            WRADDR_WAIT: begin
                wresp_valid_reg <= 1'b0;         // Writing is done
                wa_ready_reg <= 1'b1;  // Set the Ready reg on Write Address channel, 
                                       //we're waiting for the address

                if(wa_valid_reg)       // Waiting for the address to be stable
                  begin
                    w_next_state <= WRDATA_WAIT;
                    wa_addr_reg   <= wa_addr;       // Address sampling
                  end
                else
                  begin
                    w_next_state <= WRADDR_WAIT;
                  end
            end
            WRDATA_WAIT: begin
                wa_ready_reg <= 1'b0;         // Address is sampled
                wd_ready_reg <= 1'b1;  //Set the Ready reg on Write Data channel, 
                                       //we're waiting for the data
                if(wd_valid_reg)       // Waiting for the data to be stable
                  begin
                    w_next_state <= WRITE;
                    wd_data_reg   <= wd_data;   // Data sampling
                    wd_byte_en_reg <= wd_byte_en;
                  end
                else
                  begin
                    w_next_state <= WRDATA_WAIT;
                  end
            end
            WRITE: begin
                wd_ready_reg <= 1'b0;     // Data is sampled
                w_en_a2s_reg = 1'b1;
                if (w_ack_s2a)
                  begin 
                    w_next_state <= WRRESP_SEND;
                  end
                else
                  begin
                     w_next_state <= WRITE;
                  end
            end
            WRRESP_SEND: begin
                w_en_a2s_reg = 1'b0;
                wresp_bresp_reg <= 2'b00;
                wresp_valid_reg <= 1'b1;         // Writing is done
                if (wresp_ready_reg)
                  begin
                    w_next_state <= WRADDR_WAIT;
                  end
                else
                  begin
                    w_next_state <= WRRESP_SEND;
                  end
            end
        endcase    
    end

/* Signs to the BFM from AXI */
assign wa_ready = wa_ready_reg;  // Updating output
assign wd_ready = wd_ready_reg;  // Updating output
assign wresp_valid = wresp_valid_reg;  // Updating output
assign wresp_bresp = wresp_bresp_reg; // Updating output

/* From AXI to SPI */
assign w_addr_a2s = wa_addr_reg;
assign w_data_a2s = wd_data_reg;
assign byte_en_a2s = wd_byte_en_reg;
assign w_en_a2s = w_en_a2s_reg;




/* Read state machine */
    /* States */
    parameter RDADDR_WAIT = 2'b00;
    parameter READ        = 2'b01;
    parameter RDDATA_SEND = 2'b11;
    /* 10 state code is not valid */
    
    /* Registers */
    reg  [1:0] r_state_reg;
    reg  [1:0] r_next_state;
    
    reg [31:0] ra_addr_reg;           // Input buffer register
    reg        ra_valid_reg = 1'b0;   // Input buffer register
    reg        rd_ready_reg = 1'b0;   // Output buffer register
    reg        r_en_a2s_reg = 1'b0;   // Output buffer register
    reg [31:0] rd_data_reg;           // Output buffer register
    reg        ra_ready_reg = 1'b0;   // Output buffer register
    reg        rd_valid_reg = 1'b0;   // Output buffer register
    reg [1:0]  rd_rresp_reg = 2'b00;  // Output buffer register
    
    /* State register */
    always@(posedge clk)
    begin
        if(rst)
            r_state_reg <= RDADDR_WAIT;
        else
            r_state_reg <= r_next_state;
    end
    
    /* State changes (combinational logic) */
    always@(*)
    begin
        case(r_state_reg)
            RDADDR_WAIT: begin
              rd_valid_reg <= 1'b0;         // Clearing the valid sign of the previous reading cycle
              ra_ready_reg <= 1'b1;         // Set the Ready reg on Read Address channel, 
                                            // we're waiting for the address
                if(ra_valid_reg)            // Waiting to the address to be stable
                  begin
                    r_next_state <= READ;
                    ra_addr_reg  <= ra_addr;      // Address sampling
                    ra_ready_reg <= 1'b0;         // Clearing the read address ready
                  end
                else
                  begin
                    r_next_state <= RDADDR_WAIT;
                  end
            end
            READ: begin
                r_en_a2s_reg = 1'b1;
                if (r_ack_s2a)
                  begin
                    rd_data_reg <= r_data_s2a;
                    rd_rresp_reg <= 2'b00;
                    r_next_state <= RDDATA_SEND;
                  end
                else
                  begin
                    r_next_state <= READ;
                  end
            end
            RDDATA_SEND: begin
                r_en_a2s_reg = 1'b0;     
                    if(rd_ready_reg)                  // Waiting for the master to read our data
                      begin
                        rd_valid_reg <= 1'b1;
                        r_next_state <= RDADDR_WAIT;
                  end
                else
                  begin
                    r_next_state <= RDDATA_SEND;
                  end
            end
        endcase
    end


assign rd_data = rd_data_reg;
assign r_addr_a2s = ra_addr_reg;
assign r_en_a2s = r_en_a2s_reg;
assign ra_ready = ra_ready_reg;  // Updating output
assign rd_valid = rd_valid_reg;  // Updating output
assign rd_rresp = rd_rresp_reg;
    
endmodule